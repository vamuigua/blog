---
title: My First Ruby Article
date: 2020-05-27
tags: example, ruby, exploration
---

This is my first ruby article. This blog has been generated using [middleman](https://middlemanapp.com/basics/blogging/).

![My Photo](/blog/images/middleman.png)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

You probably want to delete it and write your own articles!
