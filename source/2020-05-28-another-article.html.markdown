---
title: Tommorrow's Article
date: 2020-05-28
tags: example, ruby, exploration
---

This is tommorrow's article. This blog has been generated using [middleman](https://middlemanapp.com/basics/blogging/).

![My Photo](/blog/images/middleman.png)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

You probably want to delete it and write your own articles!
